const Orders = require('../models/orders');

// this is you secret key, use test key for development and live key for production
const stripe = require("stripe")("sk_test_GwufAlp024lgCMTT4FIMjYCP00lQ6AxOp9");
const updateStock = require("../helpers/updateStock")
const sendOrderMail = require("../helpers/sendOrderMail")



class OrdersController {

    // GET FIND ALL
    // async findAll(req, res) {
    //     try {
    //         const orders = await Orders.find({});
    //         res.send(orders);
    //     }
    //     catch (e) {
    //         res.send({ e })
    //     }
    // }

    async create(req, res) {
        //console.log(req.body)
        const { name, lastName, email, address, postCode, country, state, mobile, phone, cart } = req.body.sendShippingData;
        //console.log(Date())
        try {
            let newCart = []
            //loopamos en array de cart q recibimos y metemos en newCart
            console.log(cart)
            cart.forEach( ele  => newCart.push({_id:ele._id, quantity:ele.quantity, img_link:ele.img_link, name:ele.name , price:ele.price}))
            //count allows us to create incremental order id. Orders.count counts how many entries we have in order collection
            let count      = await Orders.count()
           
            const newOrder = await Orders.create({
                order_id: count+1,
                name: name,
                lastName: lastName,
                email: email,
                address: address,
                postCode: postCode,
                country: country,
                state: state,
                mobile: mobile,
                phone: phone,
                cart:newCart
            });

            //llamamos funcion y pasamos como argmumento res y cart
            //hacemos el res.send en updateStock
            updateStock(res,cart,newOrder)
            sendOrderMail(email,name,newCart,count+1)
            //res.send({ newOrder })
        }
        catch (error) {
            //REMEMBER TO ALWAYS CONSOLE LOG
            console.log(error)
            res.send(error)
        }
    }


    async pay(req, res) {
        console.log('pay=================================', req.body)
        stripe.charges.create(req.body, postStripeCharge(res));

    }
}

const postStripeCharge = res => (stripeErr, stripeRes) => {
    if (stripeErr) {
        res.status(500).send({ error: stripeErr });
    } else {
        res.status(200).send({ success: stripeRes });
    }








    // // update product '/update'
    // async update(req, res) {
    //     let { id, prod_id, name, price, stock, material, color, dimensions, weight, description_long, description_short, img_link, category } = req.body;
    //     try {
    //         const product = await Products.find({ _id: id });
    //         if (product.length === 0) return res.send("Product doesn't exist")
    //         console.log(product)
    //         const updated = await Products.updateOne({ _id: id },
    //             {
    //                 $set: {
    //                     //si no ponemos nada nos pondria null, con esto evitamos q si esta vacio nos deje movie[0].title 
    //                     name: name || products[0].name,
    //                     prod_id: prod_id || products[0].prod_id,
    //                     price: price || products[0].price,
    //                     stock: stock || products[0].stock,
    //                     material: material || products[0].material,
    //                     color: color || products[0].color,
    //                     dimensions: dimensions || products[0].dimensions,
    //                     weight: weight || products[0].weight,
    //                     description_long: description_long || products[0].description_long,
    //                     description_short: description_short || products[0].description_short,
    //                     img_link: img_link || products[0].img_link,
    //                     category: category || products[0].category
    //                 }
    //             });
    //         res.send({ updated });
    //     }
    //     catch (error) {
    //         res.send({ error });
    //     };
    // }

    // async delete(req, res) {
    //     let { id } = req.body;
    //     try {
    //         const removed = await Products.remove({ _id: id });
    //         res.send({ removed })
    //     }
    //     catch (error) {
    //         res.send({ error });
    //     }
    // }



    // async findById(req, res) {
    //     //los req.params.itemsId es la id del producto -itemsId route path name
    //     // res.send(req.params.itemsId)
    //     var stock;
    //     try {
    //         //buscamos en el db el id del proudcto.
    //         const productInfo = await Products.findOne({ _id: req.params.itemsId });
    //         if (productInfo.stock <= 0) stock = false
    //         stock = true
    //         // const productInfo tendra todos los datos del product con id
    //         res.send({ ok: true, productInfo, stock });
    //         //al cliente enviamos el productinfo asi podremos set el state del componente
    //     }
    //     catch (e) {
    //         res.send({ e, ok: false })
    //     }
    // }

    // async findByIdSearch(req, res) {
    //     //los req.params.itemsId es la id del producto -itemsId route path name
    //     // res.send(req.params.itemsId)
    //     try {
    //         //buscamos en el db el id del proudcto.
    //         console.log('req.params.itemsId======', req.params.itemsId)
    //         const productInfo = await Products.findOne({ _id: req.params.itemsId });
    //         // const productInfo tendra todos los datos del product con id
    //         res.send({ productInfo, ok: true });
    //         //al cliente enviamos el productinfo asi podremos set el state del componente
    //     }
    //     catch (e) {
    //         res.send({ e, ok: false })
    //     }
    // }


    // async getStock(req, res) {
    //     console.log(req.params)
    //     var stock
    //     try {
    //         const productInfo = await Products.findOne({ _id: req.params.itemsId });
    //         console.log(req.params.itemsQuantity, productInfo.stock)
    //         if (productInfo.stock < req.params.itemsQuantity) {
    //             stock = false
    //         } else {
    //             stock = true
    //         }
    //         res.send({ stock, stockNumber: productInfo.stock });
    //     }
    //     catch (e) {
    //         res.send({ e, ok: false })
    //     }
    // }

    // async getShoppingCart(req, res) {
    //     //falta check availability
    //     console.log(req.body)
    //     //req body es cartItems que le paso del cliente (el array que tenemos en localStorage)
    //     try {
    //         if (req.body.cartItems) {
    //             const ids = [];
    //             //creamos un array solo con los ids
    //             req.body.cartItems.forEach(element => {
    //                 ids.push(element._id)
    //             });
    //             //$in es un metodo de mongo con in buscamos todos los productos que tengan el id en la coleccion de products.
    //             const shoppingCartItemsInfo = await Products.find({ _id: { $in: ids } });
    //             const stock = shoppingCartItemsInfo.stock;
    //             // console.log(shoppingCartItemsInfo)
    //             //mando al client los productos al cliente
    //             res.send({ shoppingCartItemsInfo, stock });
    //         } else {
    //             res.send("Your Cart is Empty")
    //         }
    //     }
    //     catch (e) {
    //         res.send({ e })
    //     }
    // }

    // async getWishList(req, res) {
    //     //falta check availability
    //     console.log(req.body)

    //     //req body es wishListItems que le paso del cliente (el array que tenemos en localStorage)
    //     try {
    //         if (req.body.wishList.length > 0) {

    //             //$in es un metodo de mongo con in buscamos todos los productos que tengan el id en la coleccion de products.
    //             const wishListItemsInfo = await Products.find({ _id: { $in: req.body.wishList } });

    //             //mando al client los productos al cliente
    //             res.send({ wishListItemsInfo, ok: true });
    //         } else {
    //             res.send({ ok: true, message: "Your Wish List is Empty" })
    //         }
    //     }
    //     catch (e) {
    //         res.send({ e })
    //     }
    // }


}


module.exports = new OrdersController();
