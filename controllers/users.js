const User       = require('../models/users'); 
const bcrypt     = require('bcrypt');
const jwt        = require('jsonwebtoken');
const config     = require('../config');
const saltRounds = 10;


const register = async (req,res) => {
	const { email , password , password_2 } = req.body;

	if( !email || !password || ! password_2) return res.send({ok:false, message:'all field are required'});
    if(  password !== password_2) return res.send({ok:false, message:'passwords must match'});
    try{
    	const user = await User.findOne({ email })
    	if( user ) return res.send({ ok:false, message:'user already exist'});
    	const hash = await bcrypt.hash(password, saltRounds)
        console.log('hash =' , hash)
        const newUser = {
        	email,
        	password : hash
        }
        const create = await User.create(newUser)
        res.send({ok:true,message:'successfully register'})
    }catch( error ){
    	res.send({error})
    }
}

const login = async (req,res) => {
    const { email , password } = req.body;
    console.log(req.body)
	if( !email || !password ) return res.send({message:'all field are required'});
	try{
    	const user = await User.findOne({ email });
    	if( !user ) return res.send({message:'plase provide a valid email'});
        const match = await bcrypt.compare(password, user.password);
        if(match) {
           const token = jwt.sign(user.toJSON(), config.secret ,{ expiresIn:100080 });
           res.send({ok:true,token,email}) 
        }else return res.send({message:'invalid password'})
        
    }catch( error ){
    	res.send({error})
    }
}

const verifyToken = (req,res) => {
  console.log(req.params.token)
    try{
       const { token } = req.params;
       const decoded   = jwt.verify(token, config.secret);
       //Decoded tiene todos los elementos del user
       console.log('decoded =',decoded)
       res.send({ok:true, message:'user exists'})
    }catch(error){
       res.send({ok:false, error})
    }
}

module.exports = { register , login , verifyToken }
