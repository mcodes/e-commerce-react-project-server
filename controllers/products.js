const Products = require('../models/products');


class ProductsController {
    // GET FIND ALL
    async findAll(req, res) {
        try {
            const products = await Products.find({});
            res.send(products);
        }
        catch (e) {
            res.send({ e })
        }
    }

    async create(req, res) {
        console.log('req.body===========================================================',req.body, 'END OF REQ.BODY')
        let { prod_id, name, price, stock, material, color, dimensions, weight, description_long, description_short, img_link, category } = req.body;
        try {
            const newProduct = await Products.create({
                prod_id: prod_id,
                name: name,
                price: price,
                stock: stock,
                material: material,
                color: color,
                dimensions: dimensions,
                weight: weight,
                description_long: description_long,
                description_short: description_short,
                img_link: img_link,
                category: category,
            });
            res.send({ newProduct })
        }
        catch (error) {
            res.send(error)
        }
    }



    // update product '/update'
    async update(req, res) {
        let { id, prod_id, name, price, stock, material, color, dimensions, weight, description_long, description_short, img_link, category } = req.body;
        try {
            const product = await Products.find({ _id: id });
            if (product.length === 0) return res.send("Product doesn't exist")
            console.log(product)
            const updated = await Products.updateOne({ _id: id },
                {
                    $set: {
                        //si no ponemos nada nos pondria null, con esto evitamos q si esta vacio nos deje movie[0].title 
                        name: name || products[0].name,
                        prod_id: prod_id || products[0].prod_id,
                        price: price || products[0].price,
                        stock: stock || products[0].stock,
                        material: material || products[0].material,
                        color: color || products[0].color,
                        dimensions: dimensions || products[0].dimensions,
                        weight: weight || products[0].weight,
                        description_long: description_long || products[0].description_long,
                        description_short: description_short || products[0].description_short,
                        img_link: img_link || products[0].img_link,
                        category: category || products[0].category
                    }
                });
            res.send({ updated });
        }
        catch (error) {
            res.send({ error });
        };
    }

    async delete(req, res) {
        let { id } = req.body;
        try {
            const removed = await Products.remove({ _id: id });
            res.send({ removed })
        }
        catch (error) {
            res.send({ error });
        }
    }



    async findById(req, res) {
        //los req.params.itemsId es la id del producto -itemsId route path name
        // res.send(req.params.itemsId)
        var stock;
        try {
            //buscamos en el db el id del proudcto.
            const productInfo = await Products.findOne({ _id: req.params.itemsId });
            if (productInfo.stock <= 0) stock = false
            stock = true
            // const productInfo tendra todos los datos del product con id
            res.send({ ok: true, productInfo, stock });
            //al cliente enviamos el productinfo asi podremos set el state del componente
        }
        catch (e) {
            res.send({ e, ok: false })
        }
    }

    async findByIdSearch(req, res) {
        //los req.params.itemsId es la id del producto -itemsId route path name
        // res.send(req.params.itemsId)
        try {
            //buscamos en el db el id del proudcto.
            console.log('req.params.itemsId======', req.params.itemsId)
            const productInfo = await Products.findOne({ _id: req.params.itemsId });
            // const productInfo tendra todos los datos del product con id
            res.send({ productInfo, ok: true });
            //al cliente enviamos el productinfo asi podremos set el state del componente
        }
        catch (e) {
            res.send({ e, ok: false })
        }
    }


    async getStock(req, res) {
        console.log(req.params)
        var stock
        try {
            const productInfo = await Products.findOne({ _id: req.params.itemsId });
            console.log(req.params.itemsQuantity, productInfo.stock)
            if (productInfo.stock < req.params.itemsQuantity) {
                stock = false
            } else {
                stock = true
            }
            res.send({ stock, stockNumber: productInfo.stock });
        }
        catch (e) {
            res.send({ e, ok: false })
        }
    }

    async getShoppingCart(req, res) {
        //falta check availability
        console.log(req.body)
        //req body es cartItems que le paso del cliente (el array que tenemos en localStorage)
        try {
            if (req.body.cartItems) {
                const ids = [];
                //creamos un array solo con los ids
                req.body.cartItems.forEach(element => {
                    ids.push(element._id)
                });
                //$in es un metodo de mongo con in buscamos todos los productos que tengan el id en la coleccion de products.
                const shoppingCartItemsInfo = await Products.find({ _id: { $in: ids } });
                const stock = shoppingCartItemsInfo.stock;
                // console.log(shoppingCartItemsInfo)
                //mando al client los productos al cliente
                res.send({ shoppingCartItemsInfo, stock });
            } else {
                res.send("Your Cart is Empty")
            }
        }
        catch (e) {
            res.send({ e })
        }
    }

    async getWishList(req, res) {
        //falta check availability
        console.log(req.body)

        //req body es wishListItems que le paso del cliente (el array que tenemos en localStorage)
        try {
            if (req.body.wishList.length > 0) {

                //$in es un metodo de mongo con in buscamos todos los productos que tengan el id en la coleccion de products.
                const wishListItemsInfo = await Products.find({ _id: { $in: req.body.wishList } });

                //mando al client los productos al cliente
                res.send({ wishListItemsInfo, ok: true });
            } else {
                res.send({ ok: true, message: "Your Wish List is Empty" })
            }
        }
        catch (e) {
            res.send({ e })
        }
    }

}


module.exports = new ProductsController();
