const styles = require('./orderMailBodyStyles')

let total = 0

module.exports = (newCart, order_id, name) => {
    return `<div style=${styles.container}>
    <h1 style=${styles.greeting}>Hi ${name}! Thank you for your order!</h1>
    <h2>Your order number is ${order_id}</h2>
  ${  //js
    newCart.map(el => {
        total+=el.price*el.quantity
        return `<div style=${styles.cartItem} ><h4>${el.name}</h4>
            <h4>${el.quantity}</h4>
            <h3>${el.price}</h3>
            <img src=${el.img_link} style=${styles.img} alt=${el.name}/></div>`
    })
}

    <h1>${total}</h1>
    <h1>Thank you!</h1>
    </div>`

}


//if we are only returning one thing we can return inline hence we would change the { return blabla} to (  blabla ) and avoid using return