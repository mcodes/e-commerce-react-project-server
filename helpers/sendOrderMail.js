const config = require('../emailConfig')
const orderMailBody = require('./orderMailBody')

//recibiendo info del order q son los argumentos de la funcion
const email = (email, name, newCart, order_id) => {
    // selecting mail service and authorazing with our credentials
    const transport = nodemailer.createTransport({
        service: 'Gmail',
        auth: {
            user: config.user,
            pass: config.pass
        },
    });

    const mailOptions = {
        to: email,
        subject: "Your order has been received!",
        // composing body of the email in a function q llamo y paso los argumentos q necesito
        html: orderMailBody(newCart, order_id, name)
    };

    transport.sendMail(mailOptions, (error, info) => {
        if (error) {
            console.log(error);
            // return res.redirect('/contacts')
        }
        console.log(`Message sent: ${info.response}`);
        // res.redirect('/contacts')

    });

}

module.exports = email



