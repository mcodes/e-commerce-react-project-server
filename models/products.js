// Schema is the way in which Mongoose controls what kind of data should be added to the DB. A single DB instance contains several Collections of objects. For each Collection we need to create a new schema.

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const productsSchema = new Schema({
    prod_id: {
        type : Number, 
        required : true,
        unique: true
    },
    name: {
        type : String, 
        required : true,
        unique: true
    },
    price: {
        type : Number, 
        required : true,
    },
    stock: {
        type : Number, 
        required : true,
    },
    material: {
        type : String, 
        required : true,
    },
    color: {
        type : String, 
    },
    dimensions: {
        type : String, 
    },
    weight: {
        type : Number, 
    },
    description_long: {
        type : String, 
    },
    description_short: {
        type : String, 
    },
    img_link: {
        type : String, 
    },
    category:{
        type : String,
        required : true,
    }
})
module.exports = mongoose.model('products', productsSchema);
