// Schema is the way in which Mongoose controls what kind of data should be added to the DB. A single DB instance contains several Collections of objects. For each Collection we need to create a new schema.

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ordersSchema = new Schema({
    order_id: {
        type: Number,
        required: true,
        unique: true
    },
    name: {
        type: String,
        required: true,
    },
    lastName: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required: true
    },
    address: {
        type: String,
        required: true,
    },
    postCode: {
        type: String,
        required: true,
    },
    country: {
        type: String,
        required: true,
    },
    state: {
        type: String,
        required: true,
    },
    mobile: {
        type: Number,
        required: true,
    },
    phone: {
        type: Number,
    },
    
    created : { 
        type: Date, default: Date.now 
    },
    cart: {
        type: Object,
        required: true
    }

})

//name of collection
module.exports = mongoose.model('userorders', ordersSchema);
