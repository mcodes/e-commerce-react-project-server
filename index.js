const express = require('express');
const app = express();
const cors = require('cors');

//bodyparser to take the data from the body
//================== body parser ================
const bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

//connect database
//================== mongoose ================
const mongoose = require('mongoose')
//productsDB = name for db's instance name
mongoose.connect('mongodb://127.0.0.1/productsDB', ()=>{
    console.log('connected to mongodb');
})

app.use(cors());



// this is you secret key, use test key for development and live key for production
// var stripe = require("stripe")("sk_test_GwufAlp024lgCMTT4FIMjYCP00lQ6AxOp9");

// app.post("/purchase", function(req, res){
// })


//================== routes ================
const productsRoutes = require('./routes/products');
const usersRoutes = require('./routes/users');
const ordersRoutes = require('./routes/orders');


//all routes in this file will be accessed 
//products will be the prefix, the routes will be added from /products onwards
app.use('/products', productsRoutes);
app.use('/users', usersRoutes);
app.use('/orders', ordersRoutes);



//we check if server is running with a callback
const PORT = process.env.PORT || 8080
app.listen( PORT, () => { 
    console.log('Server is running on port 8080') 
})

//instalamos package para q server nos deje hacer llamadas a diferentes requests to diff ports
// npm i cors --save
// const cors = require('cors');
