const express = require('express'),
    router = express.Router(),
    controller = require('../controllers/products');


//  == This route will give us back all products: ==  //
router.get('/', controller.findAll);
router.post('/create', controller.create);
router.post('/update', controller.update);
router.post('/delete', controller.delete);
//esta ruta la creamos para fetch la info de la db
router.get('/:itemsId', controller.findById);
router.get('/admin/update/:itemsId', controller.findByIdSearch);
router.get('/stock/:itemsId/:itemsQuantity', controller.getStock);
router.post('/cart_items', controller.getShoppingCart);
router.post('/wish_list_items', controller.getWishList);





module.exports=router;

