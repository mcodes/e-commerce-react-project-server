const router = require('express').Router();
const controller = require('../controllers/users')

router.post('/register',controller.register);
router.post('/login',controller.login);
router.get('/verifyToken/:token',controller.verifyToken);

module.exports = router;
